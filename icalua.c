#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <luajit-2.0/lua.h>
#include <libical/ical.h>
#include <libical/icalparameter.h>
#include <curl/curl.h>

#define SCAN_CONTINUE 0
#define SCAN_MATCH 1
#define SCAN_FAIL -1

typedef struct stringbuf {
	char *cursor;
	size_t space;
	char *pad;
	} STRINGBUF;

struct match_window {
	icaltimetype dtstart;
	icaltimetype dtend;
	};

static icaltimezone *local_zone;

static STRINGBUF *makebuf(size_t size) {
	STRINGBUF *buf = (STRINGBUF *)malloc(sizeof(STRINGBUF));
	buf->pad = (char *)malloc(size + 1);
	if (buf == NULL) {
		fprintf(stderr, "couldn't alloc %lu buffer bytes", size);
		return NULL;
		}
	buf->space = size;
	buf->cursor = buf->pad;
	return buf;
	}

static void freebuf(STRINGBUF *buf) {
	free(buf->pad);
	free(buf);
	return;
	}

static int catbuf(STRINGBUF *buf, char *contents, size_t size) {
	char *ptr = realloc(buf->pad, buf->space + size + 1);
	if (ptr == NULL) {
		fprintf(stderr, "catbuf: out of memory");
		return 0;
		}
	buf->pad = ptr;
	buf->cursor = ptr;
	memcpy(&(buf->pad[buf->space]), contents, size);
	buf->space += size;
	buf->pad[buf->space] = '\0';
	return 1;
	}

static size_t writemem(void *contents, size_t size,
		size_t nmemb, void *data) {
	size_t rsize = size * nmemb;
	if (catbuf((STRINGBUF *)data, contents, rsize)) return rsize;
	return 0;
	}

static int check_event(struct icaltimetype tstart,
		struct icaltimetype tend, void *userdata) {
	struct icaltimetype stamp = icaltime_from_string((const char *)userdata);
	if (!icaltime_is_utc(stamp)) {
		icaltimezone_convert_time(&stamp,
			local_zone, icaltimezone_get_utc_timezone());
		}
	if (!icaltime_is_utc(tstart)) {
		icaltimezone_convert_time(&tstart,
			local_zone, icaltimezone_get_utc_timezone());
		}
	if (!icaltime_is_utc(tend)) {
		icaltimezone_convert_time(&tend,
			local_zone, icaltimezone_get_utc_timezone());
		}
	// overrun into future, stop searching
	if (icaltime_compare(tstart, stamp) == 1) {
//		printf("FUTURE\n");
		return SCAN_FAIL;
		}
	// in the past or ending now, keep searching
	if (icaltime_compare(tend, stamp) != 1) {
//		printf("PAST\n");
		return SCAN_CONTINUE;
		}
	// in the pocket
//	printf("HIT\n");
	return SCAN_MATCH;
	}

static int iterate(const char *ref_start, const char *ref_end,
		const char *recur_id, const char *rrule,
		int (*callback)(struct icaltimetype, struct icaltimetype, void*),
		void *userdata, struct match_window *window) {
	int res;
	if (rrule == NULL) {
		icaltimetype dtstart = icaltime_from_string(ref_start);
		icaltimetype dtend = icaltime_from_string(ref_end);
		if (recur_id) {
			icaltimetype recur = icaltime_from_string(recur_id);
			res = (*callback)(recur, dtend, userdata);
			}
		else res = (*callback)(dtstart, dtend, userdata);
		window->dtstart = dtstart;
		window->dtend = dtend;
		return res;
		}
	struct icalrecurrencetype s_recur, e_recur;
	struct icaltimetype dtstart, dtend, dtrecur;
	int adjusted = 0;
	icalrecur_iterator *s_ritr, *e_ritr, *r_ritr;
	s_recur = icalrecurrencetype_from_string(rrule);
	e_recur = icalrecurrencetype_from_string(rrule);
	dtstart = icaltime_from_string(ref_start);
	if (recur_id) dtrecur = icaltime_from_string(recur_id);
	dtend = icaltime_from_string(ref_end);
	if ((dtend.hour == 0) && (dtend.minute == 0) && (dtend.second == 0)) {
		icaltime_adjust(&dtend, 0, 0, 0, -1);
		adjusted = 1;
		}
	s_ritr = icalrecur_iterator_new(s_recur, dtstart);
	if (recur_id) r_ritr = icalrecur_iterator_new(s_recur, dtrecur);
	else r_ritr = NULL;
	e_ritr = icalrecur_iterator_new(e_recur, dtend);
	struct icaltimetype s_next = icalrecur_iterator_next(s_ritr);
	struct icaltimetype e_next = icalrecur_iterator_next(e_ritr);
	struct icaltimetype r_next;
	if (recur_id) r_next = icalrecur_iterator_next(r_ritr);
	struct icaltimetype e_copy;
	res = SCAN_FAIL;
	while (!icaltime_is_null_time(s_next) &&
				!icaltime_is_null_time(e_next)) {
		e_copy = e_next;
		if (adjusted) icaltime_adjust(&e_copy, 0, 0, 0, 1);
		if (recur_id) res = (*callback)(r_next, e_copy, userdata);
		else res = (*callback)(s_next, e_copy, userdata);
		if (res == SCAN_MATCH) {
			window->dtstart = s_next;
			window->dtend = e_copy;
			break;
			}
		if (res != SCAN_CONTINUE) break;
		s_next = icalrecur_iterator_next(s_ritr);
		e_next = icalrecur_iterator_next(e_ritr);
		if (recur_id) r_next = icalrecur_iterator_next(r_ritr);
		}
	icalrecur_iterator_free(s_ritr);
	icalrecur_iterator_free(e_ritr);
	if (r_ritr) icalrecur_iterator_free(r_ritr);
	return res;
	}

static char *property_value(lua_State *L, const char *propname) {
	// args: event properties table
	lua_pushstring(L, propname);
	lua_gettable(L, -2);
	if (lua_isnil(L, -1)) { // no such property
		lua_pop(L, 1);
		return NULL;
		}
	lua_pushstring(L, "value");
	lua_gettable(L, -2);
	if (lua_isnil(L, -1)) {
		lua_pop(L, 2);
		return NULL;
		}
	char *value = strdup(lua_tostring(L, -1));
	lua_pop(L, 2);
	return value;
	}

static int lua_ical_event_check(lua_State *L) {
	// is timestamp encompassed by this event's window?
	// args: event object, time string
	char *stamp = strdup(lua_tostring(L, -1));
//printf("STAMP %s\n", stamp);
	lua_pop(L, 1);
	lua_pushstring(L, "properties");
	lua_gettable(L, -2);
	char *dtstart = property_value(L, "dtstart");
//printf("DTSTART %s\n", dtstart);
	char *dtend = property_value(L, "dtend");
//printf("DTEND %s\n", dtend);
	char *rrule = property_value(L, "rrule");
//printf("RRULE %s\n", rrule);
	char *recur_id = property_value(L, "recurrence_id");
	struct match_window window;
	int res = iterate(dtstart, dtend, recur_id, rrule, check_event,
		(void *)stamp, &window);
	free(rrule);
	free(dtstart);
	free(recur_id);
	free(dtend);
	free(stamp);
	lua_pop(L, 1); // properties table
	if (res == SCAN_MATCH) {
//		printf("\t\tITERATE MATCH\n");
		lua_pushstring(L, "dtstart_window");
		lua_pushstring(L, icaltime_as_ical_string(window.dtstart));
		lua_settable(L, -3);
		lua_pushstring(L, "dtend_window");
		lua_pushstring(L, icaltime_as_ical_string(window.dtend));
		lua_settable(L, -3);
		}
	else {
		lua_pop(L, 1);
		lua_pushboolean(L, 0);
		}
	return 1;
	}

static int lua_ical_calendar_check(lua_State *L) {
	// Check timestamp against all events in calendar, returning
	// a list of events that encompass that timestamp.
	// args: calendar object, time string
	int count;
	char *stamp = strdup(lua_tostring(L, -1));
	lua_pop(L, 1);
	lua_pushstring(L, "events");
	lua_gettable(L, -2);
	lua_remove(L, -2);
	lua_newtable(L);
	lua_insert(L, -2);
	lua_pushnil(L);
	count = 1;
	while (lua_next(L, -2)) {
		lua_pushstring(L, stamp);
		lua_ical_event_check(L);
		if (lua_isboolean(L, -1)) lua_pop(L, 1);
		else {
			lua_pushinteger(L, count);
			lua_insert(L, -2);
			lua_settable(L, -5);
			count += 1;
			}
		}
	lua_pop(L, 1); // calendar object
	free(stamp);
	return 1;
	}

static STRINGBUF *slurp_url(const char *url) {
	CURL *curl_handle = curl_easy_init();
	if (curl_handle == NULL) {
		fprintf(stderr, "couldn't make curl handle: %s", url);
		return NULL;
		}
	STRINGBUF *buf = makebuf(1);
	buf->space = 0;
	curl_easy_setopt(curl_handle, CURLOPT_URL, url);
	curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, writemem);
	curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, (void *)buf);
	curl_easy_setopt(curl_handle, CURLOPT_USERAGENT, "libcurl-agent/1.0");
	CURLcode res = curl_easy_perform(curl_handle);
	curl_easy_cleanup(curl_handle);
	if (res != CURLE_OK) {
		fprintf(stderr, "couldn't load from '%s'", url);
		freebuf(buf);
		return NULL;
		}
	return buf;
	}

static icalcomponent *load_from_url(const char *url) {
	STRINGBUF *buf = slurp_url(url);
	if (buf == NULL) return NULL;
	icalcomponent *component = icalparser_parse_string(buf->pad);
	freebuf(buf);
	if (component == NULL) fprintf(stderr, "FAILED TO LOAD CALENDAR from %s", url);
	return component;
	}

static char *downcase(const char *src) {
	int len = strlen(src);
	char *buf = malloc(len + 1);
	char *bufpt = buf;
	const char *srcpt = src;
	while (*srcpt != '\0') *bufpt++ = tolower(*srcpt++);
	*bufpt = '\0';
	return buf;
	}

static int lua_load_calendar(lua_State *L) {
	const char *url = lua_tostring(L, -1);
	lua_pop(L, 1);
	icalcomponent *calendar = load_from_url(url);
	if (calendar == NULL) {
		fprintf(stderr, "couldn't load calendar at %s\n", url);
		lua_pushnil(L);
		return 1;
		}
	icalcomponent *event;
	icalproperty *prop;
	icalparameter *param;
	char *key;
	lua_newtable(L); // calendar root
	lua_pushstring(L, "source");
	lua_pushstring(L, url);
	lua_settable(L, -3);
	lua_pushstring(L, "events");
	lua_newtable(L); // start calendar.events
	int event_count = 0;
	// iterate through events
	event = icalcomponent_get_first_component(calendar,
		ICAL_VEVENT_COMPONENT);
	while (event != NULL) {
		event_count += 1;
		lua_pushinteger(L, event_count);
		lua_newtable(L); // TOS: event
		// for each event, iterate through properties
		lua_pushstring(L, "properties");
		lua_newtable(L); // start event.properties
		prop = icalcomponent_get_first_property(event, ICAL_ANY_PROPERTY);
		while (prop != NULL) {
			key = downcase(
				icalproperty_kind_to_string(icalproperty_isa(prop)));
			lua_pushstring(L, key);
			free(key);
			lua_newtable(L); // property
			lua_pushstring(L, "value");
			lua_pushstring(L, icalproperty_get_value_as_string(prop));
			lua_settable(L, -3); // set property.value
			// for each property, iterate through parameters
			lua_pushstring(L, "params");
			lua_newtable(L); // start property.parameters
			param = icalproperty_get_first_parameter(prop,
				ICAL_ANY_PARAMETER);
			while (param != NULL) {
				key = downcase( 
					icalparameter_kind_to_string(icalparameter_isa(param)));
				lua_pushstring(L, key);
				free(key);
				lua_pushstring(L, icalparameter_get_xvalue(param));
				lua_settable(L, -3);
				param = icalproperty_get_next_parameter(prop, ICAL_ANY_PARAMETER);
				}
			// done with parameters, insert property.params
			lua_settable(L, -3);
			// done with this property, insert properties.<propname>
			lua_settable(L, -3);
			prop = icalcomponent_get_next_property(event, ICAL_ANY_PROPERTY);
			}
		// done with properties, insert event.properties
		lua_settable(L, -3);
		// add this event to events array
		lua_settable(L, -3);
		event = icalcomponent_get_next_component(calendar, ICAL_VEVENT_COMPONENT);
		}
	// done with events, insert calendar.events
	lua_settable(L, -3);
	icalcomponent_free(calendar);
	// add instance methods
	lua_pushstring(L, "find_events");
	lua_pushcfunction(L, lua_ical_calendar_check);
	lua_settable(L, -3);
	return 1;
	}

static char *read_timezone(char *buf, size_t len) {
	// try to read system timezone
	FILE *file = fopen("/etc/timezone", "r");
	if (file == NULL) return NULL;
	int n = fread(buf, 1, len, file);
	fclose(file);
	if (n < 1) return NULL;
	buf[n] = '\0'; // assure null termination
	while (--n >= 0) { // strip trailing whitespace
		if (!isspace(buf[n])) break;
		buf[n] = '\0';
		}
	return buf;
	}

static const char *get_timezone(void) {
	char *zone;
	char tzbuf[64];
	const char *tzname;
	zone = getenv("TZ");
	if (zone == NULL) zone = read_timezone(tzbuf, sizeof(tzbuf));
	if (zone == NULL) local_zone = icaltimezone_get_utc_timezone();
	else local_zone = icaltimezone_get_builtin_timezone(zone);
	tzname = icaltimezone_get_display_name(local_zone);
	if (tzname == NULL) { // invalid TZ
		local_zone = icaltimezone_get_utc_timezone();
		tzname = icaltimezone_get_display_name(local_zone);
		fprintf(stderr, "invalid timezone '%s'", zone);
		}
	return tzname;
	}

int luaopen_icalua(lua_State *L) {
	lua_newtable(L);
	lua_pushstring(L, "load_calendar");
	lua_pushcfunction(L, lua_load_calendar);
	lua_settable(L, -3);
	lua_pushstring(L, "timezone");
	lua_pushstring(L, get_timezone());
	lua_settable(L, -3);
	return 1;
	}
